<?php

namespace App\Classes\Implementations;

use App\Interfaces\Alive;


class Dog implements Alive {
    private $name;
    private $breed;
    private $age;


    public function fetch($thing) {
        echo "the dog bring the $thing back ! good boy !";
    }

    public function breath(): void
    {
        echo "I breat with my dog lungs";
    }

    public function eat($food): void
    {
        echo "I eat the $food in my bowl";
    }

    public function emitSound(): string
    {
        return "bork bork bork";
    }
}