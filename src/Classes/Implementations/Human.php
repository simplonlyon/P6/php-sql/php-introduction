<?php

namespace App\Classes\Implementations;

use App\Interfaces\Alive;

/**
 * Notre classe Human implémente l'interface Alive, celà signifie 
 * que la classe Human doit posséder (surcharger) toutes les 
 * méthodes qui sont définie dans l'interface Alive en respectant
 * à la lettre les typages des arguments d'entrées et de sortie.
 * Une classe peut implémenter autant d'interface qu'on le souhaite,
 * mais il faut garder en tête qu'à chaque nouvelle interface, il
 * faudra implémenter toutes les méthode de celle ci, on peut donc
 * assez rapidement se retrouver avec une classe avec trop de méthode
 * si on lui fait implémenter trop d'interface
 */
class Human implements Alive {
    public $name;

    public function __construct(string $name) {
        $this->name = $name;
    }

    public function breath(): void
    {
        echo "breathing with my human lungs";
    }

    public function eat($food): void
    {
        echo "yum yum yum very good $food";
    }

    public function emitSound(): string
    {
        return "Bonjour, je suis un humain, je m'appelle humain $this->name";
    }
}