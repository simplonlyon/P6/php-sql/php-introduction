# Introduction Au PHP
Projet d'introduction au PHP pour la promo 6 de Simplon Lyon

## Création d'un projet PHP avec docker
1. On crée un dépôt gitlab, on le clone
2. On va récupérer sur https://phpdocker.io/generator ou dans un autre projet une configuration docker-compose qu'on copie-colle/décompresse dans le dossier qu'on vient de cloner (éventuellement, retirer les lignes `container_name` du fichier docker-compose.yml)
3. On ouvre un terminal dans le dossier qu'on a cloné et on exécute un `composer init`
4. On va rajouter dans le fichier composer.json la partie concernant l'autoloading de nos fichier. Pour ça on rajoute `"autoload": {"psr-4": {"App\\": "src/"}}` dans le json.
5. On lance ensuite un `composer dumpautoload` pour générer le fichier d'autoloading (cette commande est à lancer à chaque fois qu'on clone le projet ou quand on modifie la partie autoload du composer.json)
6. En environnement de développement, on voudra afficher les erreurs, on va donc devoir aller modifier la configuration de php dans le fichier phpdocker/php-fpm/php-ini-ovverides.ini pour y ajouter les lignes `display_errors = On` et `display_startup_errors = On`


## Utilisation d'un projet PHP fait de cette manière
1. Le serveur nginx pointe sur un dossier `public` qui doit se trouver à la racine de notre projet. Les fichiers à l'intérieur de ce dossier seront les seuls points d'entrée possible de l'application, les autres fichiers PHP devront être mis dans un dossier `src` et seront utilisés par les points d'entrée
2. Pour pouvoir accéder à notre PHP on doit lancer docker avec `docker-compose up` et aller sur http://localhost:8080
3. Si on a des dépendances on doit faire un `composer install` pour que composer les télécharge

### Conventions de nommage/de fichier
* Pour la POO, on reste sur le principe : une classe -> un fichier
* Les fichiers contenant des classes (ou des interfaces) doivent avoir un nom correspondant exactement au nom de la classe (CapitalizedCamelCase)
* Les dossiers à l'intérieur du src seront également en CapitalizedCamelCase pour correspondre aux namespaces
* Un fichier contenant une classe (ou une interface) doit toujours commencer par la déclaration de son namespace, qui correspond au namespace de l'application, suivit des dossiers et sous dossier où se trouve la classe séparés par des antislash
