<?php

//$_POST et $_GET plutôt déconseillés pasque pas d'échappement des caractères
//echo $_POST["thing"] . "<br/>";
//Préférable d'utiliser filter_input qui va appliquer un échappement sur l'input
//echo filter_input(INPUT_POST, "thing", FILTER_SANITIZE_SPECIAL_CHARS);

//ou alors un filter_input_array qui échappera tous les input du formulaire et le mettra sous forme d'un tableau associatif
// $form = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
// echo $form["thing"];

//Remplacer l'input thing dans le formulaire form.html 
//par 2 inputs name et surname, puis dans form.php récupérer ces
//deux valeurs pour écrire une phrase genre "Hello, I am name surname"


$form = filter_input_array(INPUT_POST, FILTER_SANITIZE_SPECIAL_CHARS);
echo $form["name"];
echo $form["surname"];

echo "Bonjour je mappelle {$form["name"]} mon pseudo est {$form["surname"]}" ;