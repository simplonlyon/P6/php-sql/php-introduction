<?php

use App\Classes\Implementations\Human;
use App\Classes\Implementations\Dog;
use App\Interfaces\Alive;

require "../vendor/autoload.php";

$instanceHuman = new Human("Johnny");

$instanceDog = new Dog();

befriend($instanceHuman);
befriend($instanceDog);

/**
 * La function befriend attend un argument de type Alive,
 * on peut donc lui donner aussi bien une instance de Dog qu'une
 * instance de Human, ou de n'importe quelle autre classe à laquelle
 * on aura fait l'implémentation de Alive.
 */
function befriend(Alive $friend) {
    /**
     * Dans la fonction, le paramètre sera considéré comme étant
     * de type Alive, c'est-à-dire que ne seront accessible que 
     * les méthodes définies dans l'interface et rien d'autre.
     * (par exemple, même si on donne un Dog en argument à la 
     * méthode, on ne pourra pas lui dire de fetch ici)
     */
    echo "me : hello my new friend !<br/>";
    echo "new friend :". $friend->emitSound();
    
}